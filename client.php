<?php
/**
 * Example client implementation for PHP 7.0 to PHP 7.2.
 *
 * If using openSSL:
 * - The IV parameter MUST be a randomly generated, alphanumeric string to assure security.
 * - If IV is specified, the system switched encryption to openSSL (if installed) and ignores the internal AES encryption.
 */

require_once			        'class.PTN_API_v2.php';

const PUBLIC_KEY = '';
const PRIVATE_KEY = '';
const USERNAME = '';
const PASSWORD = '';

try {
	$api					        = new PTN_API_v2(
		'q',                                        // Environment: 'q' (non-public testing), 'demo' (sandbox) and '' (production)
		2824,                                       // API application endpoint
		rand(1000000000000000, 9999999999999999),   // IV, leave as empty string to force non-openSSL encryption.
		constant('PUBLIC_KEY'),                     // Public key, supplied by PayThem
		constant('PRIVATE_KEY'),                    // Private key, supplied by PayThem
		constant('USERNAME'),                       // Username, supplied by PayThem
		constant('PASSWORD')                        // Password, supplied by PayThem
	);

	//$res = $api->get_AccountBalance();              // Has no parameters
	//$res = $api->get_OEMList();                     // Has no parameters
	//$res = $api->get_BrandList();                   // Has no parameters
	//$res = $api->get_ProductList();                 // Has no parameters
	//$res = $api->get_Vouchers(1252, 1, random_int(10000000, 99999999));   // Requires PRODUCT_ID, QUANTITY and REFERENCE_ID (own reference, optional, unless set to required on server side)
	//$res = $api->get_FinancialTransaction_ByDateRange(date('Y-m-d')); // Requires FROM_DATE and optional TO_DATE (default to start) with max 30 day difference
	//$res = $api->get_SalesTransaction_ByDateRange(date('Y-m-d')); // Requires FROM_DATE and optional TO_DATE (default to start) with max 30 day difference
	//$res = $api->get_ProductAvailability(1252);     // Requires PRODUCT_ID
	//$res = $api->get_AllProductAvailability();      // Has no parameters
	//$res = $api->get_ProductFormats();              // Has no parameters
	//$res = $api->get_MaxAllowedVouchersPerCall();   // Has no parameters
	//$res = $api->get_ProductInfo(1252);             // Requires PRODUCT_ID
	//$res = $api->get_SalesTransaction_ByTransactionId(1); // Requires TRANSACTION_ID
	//$res = $api->get_SalesTransaction_ByReferenceId('x');   // Requires REFERENCE_ID
	//$res = $api->get_LastSale();                    // Has no parameters

	var_export($res);
//	var_dump($api);
//	var_export($api->result);   // JSON decoded associative array.
//	var_export($api->response); // Pure response array, with SERVER_TRANSACTION_ID, SYSLOG_ID, RESULT code, ERROR_DESCRIPTION and CONTENT

} catch (Throwable $e) {
	var_dump($e->getMessage());
}