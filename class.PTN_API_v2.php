<?php
/**
 * This class is used for direct integration with the PayThem.Net Electronic Voucher Distribution API subsystem.
 *
 * This is the base class. Any calls to this must follow the API protocol inherently, without deviation.
 *
 * @contact support@paythem.atlassian.net
 * @version 7.7.1
 * @copyright PayThem.Net WLL, 2014-2024
 *
 * @requires Optional: Requires openSSL if available, else falls back to the AESCTR library.
 * @a
 */

class PTN_API_v2 {
	/**
	 * @var string $environment that defines to which PayThem VVS environment to connect to.
	 *                          Options are:
	 *                          '' = production
	 *                          'demo' = demo / sandbox
	 */
	public $environment						        = '';

	/**
	 * @var int the returned API call ID, as returned by server, representing
	 * the current call's log entry on the server.
	 */
	public $serverTransactionID					    = 0;

	/**
	 * @var string the returned API result code. 00000 if success, otherwise,
	 * a relevant error ID. Description of error in returned field ERROR_DESCRIPTION
	 */
	public $resultCode								= '';

	/**
	 * @var string $httpResponse the original HTTP response.
	 */
	public $httpResponse;

	/**
	 * @var string $httpCurlError the cURL code, if a cURL error occurs.
	 */
	public $httpCurlError;

	/**
	 * @var string $IV the initialization vector for openSSL encryption can be provided by client.
	 *                 requires to be 16 alphanumeric characters. Best practice is to randomly
	 *                 generate every time.
	 */
	public $IV								= '';

	/**
	 * @var string $errorDescription the translated error code to human legible text as returned from server.
	 */
	public $errorDescription;

	/**
	 * @var string|NULL $result the result as JSON-decoded and, optionally, decrypted, associative array.
	 */
	public $result;

	/**
	 * @var mixed|null $response the raw response, as received from server, as a string
	 */
	public $response;

	/**
	 * @var string $API_VERSION Current API Version
	 */
	private $API_VERSION                            = '2.2.8';

	/**
	 * @var string $CAPIV Current API Version
	 */
	private $CLIENT_VERSION                         = '7.7.1';

	/**
	 * @var false|resource $cURLing The cURL object, instantiated once in constructor, for multiple calls.
	 */
	private $cURLing;

	/**
	 * @var array $innerVars the inner variables for setting storage.
	 */
	private $innerVars          				    = [];

	/**
	 * @var string $hashMacENC encoding used for HMAC hash creation.
	 */
	private $hashMacENC                             = 'sha256';

	/**
	 * @var string $serverURI The URI / Universal Resource Identifier of the server.
	 */
	private $serverURI;

	/**
	 * @var bool $ERROR flag to easily identify if an error occurred from the calling function.
	 */
	private $ERROR                             = false;

	/**
	 * @var array $encryptedContent Associative array containing the encrypted content to be JSON encoded and then posted to server.
	 */
	private $encryptedContent                       = [];

	/**
	 * @var string
	 */
	public $curlErrorString;

	/**
	 * @var string
	 */
	public $curlResponseCode;

	/**
	 * @var int
	 */
	public $curlHTTPResponseCode;

	/**
	 * Constructor.
	 *
	 * @param String $environment The server environment that this transaction will be executed against.
	 * @param int    $appID       The API Application to communicate with.
	 * @param string $iv          The IV to be used for openSSL encryption. Needs to be exactly 16 alphanumerical
	 *                            characters. If no IV is passed, internal AES-CTR encryption will be used.
	 * @param string $publicKey   Public key, provided by PayThem
	 * @param string $privateKey  Private key, , provided by PayThem
	 * @param string $username    Username, provided by PayThem
	 * @param string $password    Password, provided by PayThem
	 */
	public function __construct(
			string $environment,
			int $appID=2848,
			string $iv='',
			string $publicKey='',
			string $privateKey='',
			string $username='',
			string $password=''
	) {
		if (!extension_loaded('curl')) {
			$this->ERROR                            = true;
			throw new Error('PHP curl extension not loaded.');
		}

		$this->environment                          = $environment;
		$this->serverURI            				= "https://vvs$this->environment.paythem.net/API/$appID/";
		$this->innerVars['DEBUG_OUTPUT']			= $this->innerVars['FAULTY_PROXY']
													= $this->innerVars['SERVER_DEBUG']
													= false;
		$this->innerVars['PARAMETERS']				= [];
		$this->IV                                   = $iv;

		$this->cURLing								= curl_init();
		curl_setopt($this->cURLing, CURLOPT_URL				, $this->serverURI);
		curl_setopt($this->cURLing, CURLOPT_POST    		, 1);
		curl_setopt($this->cURLing, CURLOPT_RETURNTRANSFER	, true);
		curl_setopt($this->cURLing, CURLOPT_SSL_VERIFYPEER	, true);

		$this->innerVars                            = [
			'ENCRYPT_RESPONSE'                      => true,
			'USERNAME'                              => $username,
			'PASSWORD'                              => $password,
			'PUBLIC_KEY'                            => $publicKey,
			'PRIVATE_KEY'                           => $privateKey,
			'API_VERSION'                           => $this->API_VERSION,
			'CLIENT_VERSION'                        => $this->CLIENT_VERSION,
			'CLIENT_LANGUAGE'                       => 'PHP',
			'CLIENT_LANGUAGE_VERSION'               => phpversion(),
			'SERVER_URI'                            => $this->serverURI,
			'SERVER_TIMEZONE'                       => date_default_timezone_get()
		];
	}

	/**
	 * Destructor
	 */
	public function __destruct() {
		try {
			@curl_close($this->cURLing);
		} catch (Throwable $e) {
		
		} finally {
			unset(
				$this->cURLing, $this->innerVars, $this->serverTransactionID,
				$this->resultCode, $this->httpResponse, $this->httpCurlError,
				$this->IV, $this->result
			);
		}
	}

	/**
	 * Magic method for returning from innerVars.
	 *
	 * Leaving in for backwards compatibility.
	 *
	 * @param string $prop
	 *
	 * @return mixed|null
	 */
	public function __get(string $prop) {
		return $this->innerVars[$prop] ?: NULL;
	}

	/**
	 * Magic method to set a value in innerVars.
	 *
	 * Leaving in for backwards compatibility.
	 *
	 * @param string $prop
	 * @param mixed $val
	 */
	public function __set(string $prop, $val) {
		$this->innerVars[$prop]                     = $val;
	}

	/**
	 * Magic method to determine if variable is set.
	 *
	 * Leaving in for backwards compatibility.
	 *
	 * @param string $name of variable
	 */
	public function __isset(string $name): bool {
		return isset($this->innerVars[$name]);
	}

	/**
	 * Perform the call to the server.
	 *
	 * @return array
	 * @throws Exception
	 */
	public function callAPI() {
		$this->ERROR                                = false;

		// Reset cURL results.
		$this->httpCurlError                        = '';
		$this->curlErrorString                      = '';
		$this->curlResponseCode                     = '';
		$this->curlHTTPResponseCode                 = 0;

		// Reset previously returned results;
		$this->httpResponse						    = '';
		$this->resultCode                           = '';
		$this->response                             = [
			'RESULT'                                => '',
			'SERVER_TRANSACTION_ID'                 => 0,
			'SYSLOG_ID'                             => 0,
			'ERROR_DESCRIPTION'                     => '',
			'CONTENT'                               => '',
		];
		$this->result                               = '';
		$this->errorDescription                     = '';

		// Fill in incomplete variables
		$this->setVariableDefaults('ENCRYPT_RESPONSE', $this->innerVars['ENCRYPT_RESPONSE']);

		// Check that all required variables are filled.
		$this->checkVariable('PUBLIC_KEY'			, 'API_C_00004', 'Public key cannot be empty.');
		$this->checkVariable('PRIVATE_KEY'			, 'API_C_00005', 'Private key cannot be empty.');
		$this->checkVariable('USERNAME'				, 'API_C_00006', 'Username cannot be empty.');
		$this->checkVariable('PASSWORD'				, 'API_C_00007', 'Password cannot be empty.');
		$this->checkVariable('FUNCTION'				, 'API_C_00008', 'No function specified.');

		// Build the content (POST) variable
		$content                                    = json_encode(
			array_merge(
				$this->innerVars,
				[
					'SERVER_TIMESTAMP'              => date('Y-m-d H:i:s'),
					'HASH_STUB'                     => random_int(1111111111, 9999999999),
					'PRIVATE_KEY'                   => 'REMOVED'
				]
			)
		);

		// Generate the HMAC hash
		$hash										= hash_hmac($this->hashMacENC, $content, $this->innerVars['PRIVATE_KEY']);

		// Create the headers for the POST
		$headers									= [
			'X-Public-Key: '						.$this->innerVars['PUBLIC_KEY'],
			'X-Hash: '								.$hash
		];

		// Encrypt the POST content and set the PUBLIC KEY
		$this->encryptedContent['PUBLIC_KEY']       = $this->innerVars['PUBLIC_KEY'];
		$this->encryptedContent['CONTENT']          = $this->doEncrypt($content);

		// Do cURL call
		curl_setopt($this->cURLing, CURLOPT_POSTFIELDS		, $this->encryptedContent);
		curl_setopt($this->cURLing, CURLOPT_HTTPHEADER		, $headers);
		$this->httpResponse							= curl_exec($this->cURLing);
		$this->httpCurlError                        = curl_error($this->cURLing);
		$this->encryptedContent                     = [];

		if ($this->httpCurlError) {
			$this->ERROR                            = true;
			$this->response['CONTENT']              = $this->result
													= 'ERROR';
			$this->response['RESULT']               = $this->resultCode
													= '-0001';
			$this->errorDescription                 = $this->response['ERROR_DESCRIPTION']
													= $this->httpCurlError;

			return $this->response;
		}

		// Process and decrypt the result
		$this->response								= json_decode(
			$this->httpResponse,
			true
		);
		$this->resultCode						    = $this->response['RESULT'];
		$this->serverTransactionID					= $this->response['SERVER_TRANSACTION_ID'];

		if ((string)$this->resultCode !== '00000') {
			$this->response['CONTENT']              = $this->result
													= 'ERROR';
			$this->errorDescription                 = $this->response['ERROR_DESCRIPTION'];
		} else {
			$this->result                           = $this->response['CONTENT']
													=
				json_decode(
					$this->innerVars['ENCRYPT_RESPONSE']
						? $this->doDecrypt($this->response['CONTENT'])
						: $this->response['CONTENT'],
					true
				);
		}

		return $this->response;
	}

	/**
	 * Encrypt, using supplied credentials and settings, the passed content.
	 *
	 * @param string $content
	 *
	 * @return string
	 * @throws Exception
	 */
	private function doEncrypt(string $content): string {
		// Check the IV and openSSL setup/config.
		if (
				$this->IV != '' &&
				strlen($this->IV) != 16
		) {
			throw new Exception(PHP_EOL.'IV must be exactly 16 (randomly generated) alpha-numeric.'.PHP_EOL);
		} elseif (
				strlen($this->IV) == 16 &&
				!extension_loaded('openssl')
		) {
			throw new Exception(PHP_EOL.'OPENSSL extension NOT LOADED as PHP module.'.PHP_EOL);
		}

		if ($this->IV != '') {
			$this->encryptedContent['ENC_METHOD']   = 'AES-CBC-256-OPENSSL';
			$this->encryptedContent['ZAPI']         = $this->IV;

			return base64_encode(
				openssl_encrypt(
					$content,
					'aes-256-cbc',
					$this->innerVars['PRIVATE_KEY'],
					OPENSSL_RAW_DATA,
					$this->IV
				)
			);
		} else {
			require_once 'class.Aes.php';
			require_once 'class.AesCtr.php';
			$this->encryptedContent['ENC_METHOD']   = 'AES-CTR-256-INTERNAL';

			return base64_encode(
				AesCtr::encrypt(
					$content,
					$this->innerVars['PRIVATE_KEY'],
					256
				)
			);
		}
	}

	/**
	 * Decrypt, using supplied credentials and settings, passed content.
	 *
	 * @param string $content
	 *
	 * @return string
	 */
	private function doDecrypt(string $content): string {
		return
			$this->IV == ''
				? AesCtr::decrypt(base64_decode($content), $this->innerVars['PRIVATE_KEY'], 256)
				: openssl_decrypt(base64_decode($content), 'aes-256-cbc', $this->innerVars['PRIVATE_KEY'], OPENSSL_RAW_DATA, $this->IV);
	}

	/**
	 * Check if a variable has been registered in innerVars, stop processing if not.
	 *
	 * @param string $var
	 * @param string $errCode
	 * @param string $errMsg
	 *
	 * @return void
	 * @throws Exception
	 */
	private function checkVariable(string $var, string $errCode='UNSPECIFIED', string $errMsg='UNSPECIFIED') {
		if (!isset($this->innerVars[$var])) {
			throw new Exception("\nERROR $errCode: $errMsg\n");
		}
	}

	/**
	 * Set default values for variables.
	 *
	 * @param string $var
	 * @param mixed $default
	 *
	 * @return void
	 */
	private function setVariableDefaults(string $var, $default) {
		if (!isset($this->innerVars[$var])) {
			$this->innerVars[$var]                  = $default;
		}
	}

	/**
	 * Begin API call functions
	 * From version 2.2.8
	 */

	/**
	 * Retrieve an Original Equipment Manufacturer (OEM) List
	 *
	 * @return array
	 * @throws Exception
	 */
	public function get_OEMList() {
		$this->innerVars['FUNCTION']				= __FUNCTION__;

		return $this->callAPI();
	}

	/**
	 * Get Brand List
	 *
	 * Retrieve a list of brands
	 *
	 * @return array
	 * @throws Exception
	 */
	public function get_BrandList() {
		$this->innerVars['FUNCTION']				= __FUNCTION__;

		return $this->callAPI();
	}

	/**
	 * Get Product List
	 *
	 * Retrieve a list of all products
	 *
	 * @return array
	 * @throws Exception
	 */
	public function get_ProductList() {
		$this->innerVars['FUNCTION']				= __FUNCTION__;

		return $this->callAPI();
	}

	/**
	 * Get Account Balance
	 *
	 * Retrieve the current balance of profile
	 *
	 * @return array
	 * @throws Exception
	 */
	public function get_AccountBalance() {
		$this->innerVars['FUNCTION']				= __FUNCTION__;

		return $this->callAPI();
	}

	/**
	 * Get Vouchers
	 *
	 * Purchase the specified amount of vouchers for the specified product
	 *
	 * @param int $productId The ID of the product as retrieved from get_ProductList
	 * @param int $quantity The quantity of vouchers to purchase (Defaults to 1)
	 * @param string $reference The reference ID for the transaction
	 *
	 * @return array
	 * @throws Exception
	 */
	public function get_Vouchers(int $productId, int $quantity = 1, string $reference = '') {
		$this->innerVars['FUNCTION']				= __FUNCTION__;
		$this->innerVars['PARAMETERS']['PRODUCT_ID']	= $productId;
		$this->innerVars['PARAMETERS']['QUANTITY']		= $quantity;
		$this->innerVars['PARAMETERS']['REFERENCE_ID']	= $reference;
		
		return $this->callAPI();
	}

	/**
	 * Handle date calculations for date based calls.
	 *
	 * @param string $fromDate
	 * @param string $toDate
	 *
	 * @return array
	 * @throws Exception
	 */
	private function dateBasedCall(string $fromDate, string $toDate='') {
		if (strlen($fromDate) < 10) {
			return [
				'RESULT'                            => -1,
				'CONTENT'                           => 'Incorrect from date length.'
			];
		} elseif (strlen($toDate) === 10) {
			$fromDate                               = $fromDate.' 00:00:00';
		}

		$this->innerVars['PARAMETERS']['FROM_DATE']	= $fromDate;
		$this->innerVars['PARAMETERS']['TO_DATE']	= (!empty($toDate) ? $toDate : date('Y-m-d')).' 23:59:59';

		return $this->callAPI();
	}

	/**
	 * Get Financial Transaction By Date Range
	 *
	 * Get all financial transactions (deposits, credits, reversals, purchases, etc.) for company
	 *
	 * @param string $fromDate The date from which to check, excluding the time (e.g. 2022-01-01)
	 * @param string $toDate The date to which to check, excluding the time (e.g. 2022-01-31)
	 *
	 * @return array
	 * @throws Exception
	 */
	public function get_FinancialTransaction_ByDateRange(string $fromDate, string $toDate = '') {
		$this->innerVars['FUNCTION']				= __FUNCTION__;

		return $this->dateBasedCall($fromDate, $toDate);
	}

	/**
	 * Get Sales Transaction By Date Range
	 *
	 * Get all vouchers purchased during a specific period for API user
	 *
	 * @param string $fromDate The date from which to check, excluding the time (e.g. 2022-01-01)
	 * @param string $toDate The date to which to check, excluding the time (e.g. 2022-01-31)
	 *
	 * @return array
	 * @throws Exception
	 */
	public function get_SalesTransaction_ByDateRange(string $fromDate, string $toDate = '') {
		$this->innerVars['FUNCTION']				= __FUNCTION__;

		return $this->dateBasedCall($fromDate, $toDate);
	}

	/**
	 * Get Product Availability
	 *
	 * Retrieve the availability of the specified product
	 *
	 * @param int $productId The ID of the product as retrieved from get_ProductList
	 *
	 * @return array
	 * @throws Exception
	 */
	public function get_ProductAvailability(int $productId) {
		$this->innerVars['FUNCTION']				= __FUNCTION__;
		$this->innerVars['PARAMETERS']['PRODUCT_ID']= $productId;

		return $this->callAPI();
	}

	/**
	 * Get All Product Availability
	 *
	 * Retrieve the availability of all products
	 *
	 * @return array
	 * @throws Exception
	 */
	public function get_AllProductAvailability() {
		$this->innerVars['FUNCTION']				= __FUNCTION__;

		return $this->callAPI();
	}

	/**
	 * Get Product Formats
	 *
	 * Get all financial transactions (deposits, credits, reversals, purchases, etc) for company
	 *
	 * @return array
	 * @throws Exception
	 */
	public function get_ProductFormats() {
		$this->innerVars['FUNCTION']				= __FUNCTION__;

		return $this->callAPI();
	}

	/**
	 * Get Maximum Allowed Vouchers Per Call
	 *
	 * Retrieve the maximum allowed voucher purchase per call
	 *
	 * @return array
	 * @throws Exception
	 */
	public function get_MaxAllowedVouchersPerCall() {
		$this->innerVars['FUNCTION']				= __FUNCTION__;

		return $this->callAPI();
	}

	/**
	 * Get Product Info
	 *
	 * Retrieves information about the requested product
	 *
	 * @param int $productId The ID of the product as retrieved from get_ProductList
	 *
	 * @return array
	 * @throws Exception
	 */
	public function get_ProductInfo(int $productId) {
		$this->innerVars['FUNCTION']				= __FUNCTION__;
		$this->innerVars['PARAMETERS']['PRODUCT_ID']	= $productId;

		return $this->callAPI();
	}

	/**
	 * Get Transaction By Transaction ID
	 *
	 * Retrieves all transactions for a given transaction ID
	 *
	 * @param string $transactionId The transaction ID returned during get_Vouchers purchase
	 *
	 * @return array
	 * @throws Exception
	 */
	public function get_SalesTransaction_ByTransactionId(string $transactionId) {
		$this->innerVars['FUNCTION']				= __FUNCTION__;
		$this->innerVars['PARAMETERS']['TRANSACTION_ID']	= $transactionId;

		return $this->callAPI();
	}

	/**
	 * Get Transaction By Reference ID
	 *
	 * Retrieves all transactions for a given reference ID
	 *
	 * @param string $referenceId The reference ID passed during get_Vouchers
	 *
	 * @return array
	 * @throws Exception
	 */
	public function get_SalesTransaction_ByReferenceId(string $referenceId) {
		$this->innerVars['FUNCTION']				= __FUNCTION__;
		$this->innerVars['PARAMETERS']['REFERENCE_ID']	= $referenceId;

		return $this->callAPI();
	}

	/**
	 * Get Last Sale
	 *
	 * Retrieve the information of the last sale that successfully processed
	 *
	 * @return array
	 * @throws Exception
	 */
	public function get_LastSale() {
		$this->innerVars['FUNCTION']				= __FUNCTION__;

		return $this->callAPI();
	}
}